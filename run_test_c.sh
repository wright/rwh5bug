#!/bin/bash

rm -f endtest write_log read_log* test_newname.h5

h5cc h5ex_g_iterate.c -o h5ex_g_iterate

# get the core
ulimit -c 100000000

python3 hammer.py --writer > write_log & 
sleep 1 
# Run concurrent processes
export HDF5_USE_FILE_LOCKING="FALSE"
for i in {1..2}; do
    # ( python3 hammer.py > read_log$i ) &
    ( ./h5ex_g_iterate test_newname.h5 2>&1  > read_log$i ) &
done

wait -n
echo " some reader died , killing  "
touch endtest
kill $(jobs -p)

