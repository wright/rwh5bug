#!/bin/bash

rm -f endtest write_log read_log* test_newname.h5

sleep 1

# get the core
ulimit -c 100000000

python3 hammer.py --writer > write_log & 

# Run concurrent processes
for i in {1..12}; do
    ( python3 hammer.py > read_log$i ) &
done

wait -n
echo " some reader died , killing  "
touch endtest
# kill $(jobs -p)

