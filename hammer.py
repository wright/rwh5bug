"""Test processing an HDF5 file that is being written
0;256;0c
Launch the writer (only one):

    python safe_open_hdf5.py --writer

Launch the readers (as many as you want):

    python safe_open_hdf5.py
"""

import os, sys
import time
import datetime
import h5py
import numpy
from contextlib import contextmanager


TEST_DATA = numpy.arange(7, dtype=int)


class HDF5TimeoutError(TimeoutError):
    pass


def retry(timeout=None):
    """Decorate method that open+read an HDF5 file that is being written too.
    When HDF5 IO fails (because the writer is modifying the file) the method
    will be retried.

    Note: the method needs to be idempotent.
    """

    def decorator(method):
        def wrapper(filename, *args, **kw):
            os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE"
            t0 = time.time()
            while True:
                try:
                    with h5py.File(filename, mode="r") as f:
                        return method(f, *args, **kw)
                except HDF5TimeoutError:
                    raise
                except Exception:
                    # TODO: check whether it comes from h5py
                    pass
#                print(f"retry {method}")
                time.sleep(0.1)
                if timeout is not None and (time.time() - t0) > timeout:
                    raise HDF5TimeoutError
        return wrapper
    return decorator


@retry(timeout=None)
def process_scan(h5file, scan_name):
    scan = h5file[scan_name]
    print("reading scan ", scan.name )
    if "end_time" not in scan.attrs:
        print("Scan not processed (not complete yet)")
        return
    measurement = scan["measurement"]
    for k in measurement:
        shp = measurement[k].shape # hit the metadata only ...
        d2 = measurement[k][shp[0]//2]

@retry(timeout=None)
def get_scan_names(h5file):
    return list(h5file["/"])[:-1]

def reader(filename):
    """Main reader loop
    """
    while not os.path.exists("endtest"):
        scans = get_scan_names(filename)
        print("Got scans")
        for scan_name in scans:
            process_scan(filename, scan_name)
        sys.stdout.flush()    
    
def write_scan(f, scan_name):
    print("writing scan ", scan_name)
    scan = f.create_group(scan_name)
    scan.attrs["NX_class"] = "NXentry"
    scan.attrs["start_time"] = datetime.datetime.now().isoformat()
    measurement = scan.create_group("measurement")
    measurement.attrs["NX_class"] = "NXcollection"
    for i in range(1,10):
        measurement.create_dataset("data" + str(i),
                                   data=TEST_DATA, 
                                   compression='gzip', # with compression
                                   chunks=(13,), # and stupid tiny chunks
                                   maxshape=(None,)) # and growing
        f.flush() # always flush
    for k in range(3): # and append    
        for i in range(9,0,-1):
            npts = len(measurement["data" + str(i)])
            measurement["data"+str(i)].resize( (npts + len(TEST_DATA),))
            measurement["data" + str(i)][npts:] = TEST_DATA
            f.flush()
    scan.attrs["end_time"] = datetime.datetime.now().isoformat()
    print("finished writing scan ", scan_name)
    sys.stdout.flush()
    
def writer(filename):
    """Main writer loop
    """
    os.environ["HDF5_USE_FILE_LOCKING"] = "TRUE"
    try:
        os.unlink(filename)
    except FileNotFoundError:
        pass
    while not os.path.exists("endtest"):
        with h5py.File(filename, mode="a", track_order=True) as f:
            scans = list(f["/"])
            if scans:
                scan_number = int(scans[-1].split(".")[0]) + 1
            else:
                f.attrs["NX_class"] = "NXroot"
                scan_number = 1
            scan_name = str(scan_number) + ".1"
            write_scan(f, scan_name)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="Test processing an HDF5 which is being written."
    )
    parser.add_argument(
        "--writer", action="store_true", help="Run a writer (do only once)"
    )
    args = parser.parse_args()
    if args.writer:
        writer("test_newname.h5")
    else:
        reader("test_newname.h5")
    print("Clean exit")
